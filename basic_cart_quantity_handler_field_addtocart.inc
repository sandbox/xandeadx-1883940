<?php

class basic_cart_quantity_handler_field_addtocart extends basic_cart_handler_field_addtocart {
  /**
   * Overrides basic_cart_handler_field_addtocart::render().
   */
  function render($values) {
    $build = parent::render($values);
    if ($build) {
      $build['#prefix'] .= '<input type="text" name="quantity" class="form-text" value="1" />';
      return $build;
    }
  }
}

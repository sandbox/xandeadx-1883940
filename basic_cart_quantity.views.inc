<?php

/**
 * Implements hook_views_data_alter().
 */
function basic_cart_quantity_views_data_alter(&$data) {
  $data['node']['addtocartlink']['field']['handler'] = 'basic_cart_quantity_handler_field_addtocart';
}

(function ($) {
  Drupal.behaviors.basicCartQuantity = {
    attach: function (context, settings) {
      $('.basic-cart-add-to-cart-link', context).click(function() {
        this.href += '/' + $(this).parent().find('input[name="quantity"]').val();
      });
    }
  };
})(jQuery);